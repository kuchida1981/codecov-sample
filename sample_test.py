import unittest
import sample

class SampleTestCase(unittest.TestCase):

    def test_summ(self):
        self.assertEqual(sample.summ(1, 2, 3), 6)
